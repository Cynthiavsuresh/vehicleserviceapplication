package com.example.vehicle.utils;

public class ApplicationConstants {

	
	
	//Success Message and codes
	public static final Long SUCCESS_STATUS_CODE = 200l;
	public static final String SUCCESS_STATUS_MESSAGE = "Ordered Food Successfully";
	
	//Error Messages and codes
	public static final String NO_RECORD_FOUND_MESSAGE = "No Reecord found in this page";
	public static final Long NO_RECORD_FOUND_CODE = 204l;
	public static final Long INVALID_USER_CODE = 203l;
	public static final String INVALID_USER_MESSAGE = "Please enter valid user details";
	public static final Long INVALID_FOOD_CODE = 203l;
	public static final String INVALID_FOOD_MESSAGE = "Please enter valid food details";
	public static final Long METHOD_ARGUMENT_EXCEPTION_CODE = 404l;
	public static final String METHOD_ARGUMENT_EXCEPTION_MESSAGE = "";


}
