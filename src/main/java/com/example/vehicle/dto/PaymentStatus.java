package com.example.vehicle.dto;

public enum PaymentStatus {

	PENDING, PAID
}
