package com.example.vehicle.dto;

public enum Type {
	
	THIRD_PARTY_CLAIM,
	OWN_DAMAGE_CLAIM,
	COMPREHENSIVE

}
