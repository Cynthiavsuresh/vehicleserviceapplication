package com.example.vehicle.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.example.vehicle.dto.ServiceType;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceRequest {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long serviceId;
	private ServiceType serviceType;
	@ManyToOne
	@JoinColumn(name="insuranceId")
	private Insurance insurance;
	private BigDecimal serviceCharge;
	private LocalDate serviceDate;

}
