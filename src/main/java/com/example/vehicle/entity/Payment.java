package com.example.vehicle.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.example.vehicle.dto.DamageType;
import com.example.vehicle.dto.PaymentStatus;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long paymentId;
	private DamageType damageType;
	@OneToOne
	@JoinColumn(name = "serviceId")
	private ServiceRequest serviceRequest;
	private LocalDate paymentDate;
	private PaymentStatus paymentStatus;
	private BigDecimal chargedBill;

}
