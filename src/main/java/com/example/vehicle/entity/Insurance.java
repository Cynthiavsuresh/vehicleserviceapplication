package com.example.vehicle.entity;

import java.time.LocalDate;

import com.example.vehicle.dto.Fuel;
import com.example.vehicle.dto.Type;
import com.example.vehicle.dto.VehicleType;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Insurance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long insuranceId;
	private String vehicleNumber;
	private String ownerName;
	private Type claimType;
	private VehicleType vehicleType;
	private String model;
	private Fuel fuel;
	private LocalDate insuranceStartDate;
	private LocalDate insuranceEndDate;
	private String VehicleRTO;
	
	
}
