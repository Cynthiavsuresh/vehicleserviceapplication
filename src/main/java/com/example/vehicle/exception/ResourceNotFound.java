package com.example.vehicle.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResourceNotFound extends RuntimeException {

	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	private Long Statuscode;
   

	
}
