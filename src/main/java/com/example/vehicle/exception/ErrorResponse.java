package com.example.vehicle.exception;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value=Include.NON_EMPTY)
public class ErrorResponse {

	private Map<String, String> validationMessage;
	private String message;
	private Long Statuscode;
}
