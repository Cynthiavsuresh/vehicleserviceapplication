package com.example.vehicle.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.vehicle.entity.ServiceRequest;

public interface ServiceRepository extends JpaRepository<ServiceRequest, Long>{

}
