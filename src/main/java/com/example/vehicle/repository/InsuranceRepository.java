package com.example.vehicle.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.vehicle.entity.Insurance;

public interface InsuranceRepository extends JpaRepository<Insurance, Long> {

}
