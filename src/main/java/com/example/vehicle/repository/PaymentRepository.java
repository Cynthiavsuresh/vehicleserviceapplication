package com.example.vehicle.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.vehicle.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long>{

}
